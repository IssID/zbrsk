<?php

namespace app\core\Models;

use app\core\DataBase;
use Exception;
use PDO;
use PDOStatement;

abstract class ActiveRecords
{
    /** @var array атрибуты полученные из базы */
    private $_attributes = [];
    /** @var array новые атрибуты для записи в базу */
    private $_new_attributes = [];
    /** @var mixed|string название первичного ключа */
    private $_primary_key = 'id';

    /** @var string[] ключи переменных для исключения индексации */
    const VAR_IGNORE = ['_attributes', '_new_attributes', '_primary_key'];

    /**
     * Называние таблицы
     * @return string
     */
    abstract static function tableName(): string;

    /**
     * ActiveRecords constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->execute("DESCRIBE " . static::tableName(), []);
        if (!$results = self::$stmt->fetchAll()) {
            $this->setError(404, 'Не удалось найти запись в таблице ' . static::tableName());
            return $this;
        }

        foreach ($results as $field) {
            $this->{$field['Field']} = null;
            if (!empty($field['Key']) && $field['Key'] == 'PRI') {
                $this->_primary_key = $field['Field'];
            }
        }
        return $this;
    }

    /** @var $errors array массив ошибок */
    protected static $errors = [];
    /** @var $stmt PDOStatement */
    protected static $stmt;

    /**
     * Получить ошибки
     * @return array
     */
    public function getErrors()
    {
        return self::$errors;
    }

    /**
     * @param int $code
     * @param string $message
     */
    public function setError($code = 0, $message = '')
    {
        if ($code && $message) {
            self::$errors[]['code'] = $code;
            self::$errors[]['message'] = $message;
            return;
        }

        if (isset(self::$stmt->errorInfo()[2])) {
            self::$errors[]['message'] = self::$stmt->errorInfo()[2];
            return;
        }

        self::$errors[] = self::$stmt->errorInfo();
    }

    /**
     * Получить одну запись
     *
     * @param array $params
     * @return ActiveRecords|false
     * @throws Exception
     */
    public function findOne(array $params)
    {
        $model = new static();
        $query = $this->prepareFind(static::tableName(), $params);
        if ($this->execute($query, $params)) {
            if (!$result = self::$stmt->fetch()) {
                $this->setError(404, 'Не удалось найти запись в таблице ' . static::tableName());
                return $model;
            }

            $model->load($result);
            $model->setAttributes();
            return $model;
        }
        return false;
    }

    /**
     * Найти все записи
     *
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function findAll(array $params = [])
    {
        $query = $this->prepareFind(static::tableName(), $params);
        if (!$this->execute($query, $params)) {
            return false;
        }

        $results = self::$stmt->fetchAll();
        $models = [];
        foreach ($results as $item) {
            $new = new static();
            $models[] = $new->load($item);
            $new->setAttributes();
        }

        return $models;
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool
     * @throws Exception
     */
    protected function execute(string $query, array $params)
    {
        self::$stmt = DataBase::getDb()->prepare($query);

        foreach ($params as $key => $param) {
            $param = htmlentities($param);
            self::$stmt->bindValue(':' . $key, $param);
        }
        if (!self::$stmt->execute()) {
            $this->setError();
            return false;
        }
        return true;
    }

    /**
     * @param string $table
     * @param array $params
     * @return string
     */
    protected function prepareInsert(string $table, array $params)
    {
        // удаляем primary key так как у нас авто инкремент
        $params = array_change_key_case($params);
        if (array_key_exists($this->_primary_key, $params)) {
            unset($params[$this->_primary_key]);
        }

        $str = 'INSERT INTO ' . $table . ' (';
        list($par, $insertPar, $insertVal) = self::prepareParams($params);
        return $str . $insertPar . ') VALUES (' . $insertVal . ');';
    }

    /**
     * @param string $table
     * @param array $params
     * @return string
     */
    protected function prepareUpdate(string $table, array $params)
    {
        $str = 'UPDATE ' . $table . ' SET ';
        if (isset($params[$this->_primary_key])) {
            unset($params[$this->_primary_key]);
        }
        list($par, $insertPar, $insertVal) = self::prepareParams($params);
        return $str . $par . ' WHERE ' . $this->_primary_key . '=:' . $this->_primary_key . ';';
    }

    public function getFieldsName()
    {
        $fields = [];
        foreach ($this as $key => $value) {
            if (!in_array($key, self::VAR_IGNORE)) {
                $fields[] = $key;
            }
        }
        return $fields;
    }

    /**
     * Подготовить select
     *
     * @param string $table
     * @param array $params
     * @return string
     */
    protected function prepareFind(string $table, array $params)
    {
        $str = 'SELECT ' . implode(', ', $this->getFieldsName()) . ' FROM ' . $table;
        list($par, $insertPar, $insertVal) = self::prepareParams($params);
        if ($par) {
            return $str . ' WHERE ' . $par . ';';
        }
        return $str . ';';
    }

    /**
     * @param string $table
     * @return string
     */
    protected function prepareDelete(string $table)
    {
        return 'DELETE FROM ' . $table . ' WHERE ' . $this->_primary_key . '=:' . $this->_primary_key . ';';
    }

    /**
     * @param array $params
     * @return string[]
     */
    private function prepareParams(array $params)
    {
        $par = '';
        $insertPar = '';
        $insertVal = '';
        foreach ($params as $key => $param) {
            $key = htmlentities($key);
            if (empty($insertPar)) {
                $par = $par . $key . "=:" . $key;
                $insertPar = $insertPar . $key;
                $insertVal = $insertVal . ":" . $key;
            } else {
                $par = $par . ', ' . $key . "=:" . $key;
                $insertPar = $insertPar . ', ' . $key;
                $insertVal = $insertVal . ', :' . $key;
            }
        }
        return [$par, $insertPar, $insertVal];
    }

    /**
     * Загружаем данные в модель
     *
     * @param array $params
     * @return $this|null
     */
    public function load(array $params)
    {
        if (!is_array($params)) {
            return null;
        }
        foreach ($params as $key => $param) {
            if (property_exists($this, $key)) {
                $this->{$key} = $param;
            }
        }
        $this->setNewAttributes();
        return $this;
    }

    /**
     * Устанавливаем новые атрибуты
     */
    private function setNewAttributes()
    {
        foreach ($this as $key => $value) {
            if (!in_array($key, self::VAR_IGNORE)) {
                $this->_new_attributes[$key] = $value;
            }
        }
    }

    /**
     * Устанавливаем старые атрибуты
     */
    private function setAttributes()
    {
        foreach ($this as $key => $value) {
            if (!in_array($key, self::VAR_IGNORE)) {
                $this->_attributes[$key] = $value;
            }
        }
    }

    /**
     * Сохраняем
     *
     * @return bool
     * @throws Exception
     */
    public function save()
    {
        $tableName = $this->tableName();
        if (!isset($tableName) && empty($tableName)) {
            return false;
        }
        if (!empty($this->_attributes)) {
            $query = $this->prepareUpdate($this->tableName(), $this->_new_attributes);
            return $this->execute($query, $this->_new_attributes);
        } else {
            unset($this->_new_attributes[$this->_primary_key]);
            $query = $this->prepareInsert($tableName, $this->_new_attributes);
            return $this->execute($query, $this->_new_attributes);
        }
    }
}