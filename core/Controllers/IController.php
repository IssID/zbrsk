<?php


namespace app\core\Controllers;


interface IController
{
    /**
     * Отображение ошибок
     *
     * @param $data
     * @param int|null $code
     * @return mixed
     */
    public static function showError($data, int $code = null);
}