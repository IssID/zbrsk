<?php

namespace app\core\Controllers;

use app\core\App;
use app\core\Errors;
use Exception;

abstract class Controller implements IController
{
    /** @var string title страницы */
    public $title = '';
    /** @var string Название layout */
    public $layout = 'bootstrapNav';
    /** @var string Название отображения */
    private $viewName = 'default';
    /** @var string Основной контент */
    private $content = '';
    /** @var string Название бренда (сайта) */
    public $brandName = '';

    /**
     * Отображение ошибок
     *
     * @param $data
     * @param int|null $code
     * @throws Exception
     */
    public static function showError($data, int $code = null)
    {
        Errors::showError($data, $code);
    }

    /**
     * Вывод View
     *
     * @param string $view
     * @param array $attributes
     * @throws Exception
     */
    public function render(string $view, array $attributes = [])
    {
        $this->viewName = $view;
        $this->brandName = App::$config['brandName'] ?? 'Brand';
        ob_start();
        // Генерация переменных по названию ключа
        foreach ($attributes as $key => $attribute) {
            $name_of_the_variable = $key;
            $$name_of_the_variable = $attribute;
        }

        if (file_exists(__DIR__ . "/../../views/{$this->viewName}.php")) {
            require_once __DIR__ . "/../../views/{$this->viewName}.php";
        } else {
            self::showError('Не удалось найти body страницы', 404);
        }

        $this->content = ob_get_clean();

        if (file_exists(__DIR__ . "/../../views/layouts/{$this->layout}.php")) {
            require_once __DIR__ . "/../../views/layouts/{$this->layout}.php";
        } else {
            self::showError('Не удалось найти body страницы', 404);
        }
    }

    /**
     * Получить данные из глобальной переменной $_POST
     *
     * @param null $item
     * @return array|bool|mixed
     */
    public function getPost($item = null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($item !== null) {
                if (isset($_POST[$item]) && !empty($_POST[$item])) {
                    return $_POST[$item];
                }
                return false;
            }
            return $_POST;
        }
        return false;
    }

    /**
     * Получить данные из глобальной переменной $_GET
     *
     * @return array|bool
     */
    public function getGet($item = null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            if ($item !== null) {
                if (isset($_GET[$item]) && !empty($_GET[$item])) {
                    return $_GET[$item];
                }
                return false;
            }
            return $_GET;
        }
        return false;
    }

    /**
     * Перенаправить пользователя по ссылке
     * @param string $url
     */
    public function redirect(string $url)
    {
        header('Location: ' . $url);
        exit;
    }
}
