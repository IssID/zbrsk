<?php

namespace app\core\Controllers;

use app\core\Errors;
use Exception;

abstract class ApiController implements IController
{
    /**
     * Получаем поля при использовании метода PUT
     *
     * @return array
     */
    public static function getPut()
    {
        // Извлекаем содержимое и определяем границу
        $raw_data = file_get_contents('php://input');
        $boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));

        // Получить каждую часть
        $parts = array_slice(explode($boundary, $raw_data), 1);
        $data = [];

        foreach ($parts as $part) {
            // Если это последняя часть, заканчиваем
            if ($part == "--\r\n") break;

            // Отделять контент от заголовков
            $part = ltrim($part, "\r\n");
            list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);

            // Разобрать список заголовков
            $raw_headers = explode("\r\n", $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }

            // Парсим Content-Disposition что бы получить ключ и значение
            if (isset($headers['content-disposition'])) {
                $filename = null;
                preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
                    $headers['content-disposition'],
                    $matches
                );
                list(, $type, $name) = $matches;
                isset($matches[4]) and $filename = $matches[4];

                // обрабатываем свои поля здесь
                switch ($name) {
                    // this is a file upload
                    case 'userfile':
                        file_put_contents($filename, $body);
                        break;

                    // по умолчанию для всех остальных файлов заполняется $data
                    default:
                        $data[$name] = substr($body, 0, strlen($body) - 2);
                        break;
                }
            }
        }
        return $data;
    }

    /**
     * Отображение ошибок
     *
     * @param $data
     * @param int|null $code
     * @throws Exception
     */
    public static function showError($data, int $code = null)
    {
        Errors::showApiError($data, $code);
    }
}