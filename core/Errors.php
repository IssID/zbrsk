<?php

namespace app\core;

use Exception;

class Errors
{
    /**
     * @param int $code
     * @param string $message
     * @throws Exception
     */
    public static function showError(string $message, int $code)
    {
        if ($message || $code) {
            http_response_code($code);
            echo "<h1>$code</h1>";
            echo $message;
            return;
        }
        throw new Exception('Переданы не все параметры', 400);
    }

    /**
     * @param $data
     * @param int|null $code
     * @throws Exception
     */
    public static function showApiError($data, int $code = null)
    {
        if (is_array($data)) {
            http_response_code($code);
            echo json_encode([
                'errors' => $data,
                'success' => false
            ], JSON_UNESCAPED_UNICODE);
            die();
        }

        if ($data || $code) {
            http_response_code($code);
            echo json_encode([
                'errors' => ['message' => $data],
                'success' => false
            ], JSON_UNESCAPED_UNICODE);
            die();
        }
        throw new Exception('Переданы не все параметры', 400);
    }

    /**
     * 404
     */
    public static function notFound()
    {
        http_response_code(404);
        $oldString = "<h1>Not Found</h1>\n The requested URL was not found on this server.";
        require_once __DIR__ . "/../views/layouts/404.php";
        die();
    }

    /**
     * other error
     */
    public static function Error()
    {
        require_once __DIR__ . "/../views/layouts/errors.php";
        die();
    }

    public static function debugError($error)
    {
        require_once __DIR__ . "/../views/layouts/debug.php";
    }
}