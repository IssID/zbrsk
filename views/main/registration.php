<style>
    .panel {
        position: fixed;
        width: 400px;
        top: 20%;
        left: calc(50% - 200px);
    }
</style>
<div class="container theme-showcase" role="main">

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="panel-title"><span class="glyphicon glyphicon-pencil"></span> Registration</p>
                </div>
                <div class="panel-body">
                    <form method="post" action="registration">
                        <div class="form-group">
                            <label for="exampleInput">Login</label>
                            <input required name="login" type="login" class="form-control" id="exampleInputlogin" placeholder="Login">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">Email address</label>
                            <input required name="email" type="email" class="form-control" id="exampleInputEmail"
                                   placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword">Password</label>
                            <input required name="password" type="password" class="form-control" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-default">Register</button>
                    </form>
                </div>
                <div class="panel-footer">
                    Registred? <a href="/login">Login here</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4"></div>
        <div class="col-lg-4"></div>
    </div>
</div>